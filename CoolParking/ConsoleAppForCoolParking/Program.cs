﻿using System;

namespace ConsoleAppForCoolParking
{
    class Program
    {
        public static void Main()
        {
            var parkingFacade = new ParkingFacade();

            while (true)
            {
                //TEST PROGRAM IN CONSOLE
                Console.WriteLine("Choose command:");
                Console.WriteLine("1 - Print balance of parking");
                Console.WriteLine("2 - Print current income");
                Console.WriteLine("3 - Print free places");
                Console.WriteLine("4 - Current period transactions");
                Console.WriteLine("5 - Transaction history");
                Console.WriteLine("6 - Vehicles list");
                Console.WriteLine("7 - Add vehicle");
                Console.WriteLine("8 - Remove vehicle");
                Console.WriteLine("9 - Topup vehicle");
                Console.WriteLine("0 - Exit");
                var commandToString = Console.ReadLine();
                Console.WriteLine();
                try
                {
                    int command = int.Parse(commandToString);

                    switch (command)
                    {
                        case 0:
                            return;
                        case 1:
                            parkingFacade.PrintParkingBalance();
                            break;
                        case 2:
                            parkingFacade.PrintCurrentPeriodIncome();
                            break;
                        case 3:
                            parkingFacade.PrintParkingFreePlaces();
                            break;
                        case 4:
                            parkingFacade.PrintCurrentPeriodTransactions();
                            break;
                        case 5:
                            parkingFacade.PrintTransactionHistory();
                            break;
                        case 6:
                            parkingFacade.PrintVehiclesList();
                            break;
                        case 7:
                            parkingFacade.AddVehicle();
                            break;
                        case 8:
                            parkingFacade.RemoveVehicle();
                            break;
                        case 9:
                            parkingFacade.TopUpVehicleBalance();
                            break;
                    }

                }
                catch (FormatException)
                {
                    Console.WriteLine("Incorrect command");
                }
                Console.WriteLine();
            }

        }
    }
}
