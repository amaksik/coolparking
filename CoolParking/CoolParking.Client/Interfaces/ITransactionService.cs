﻿using CoolParking.Client.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoolParking.Client.Interfaces
{
    public interface ITransactionService
    {
        Task<IEnumerable<TransactionViewModel>> GetLastTransactions();

        Task<string> GetAllTransactions();

        Task<VehicleViewModel> TopUpVehicle(string id, decimal Sum);
    }
}
