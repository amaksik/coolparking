﻿using CoolParking.BL.Models;
using CoolParking.Client.Interfaces;
using CoolParking.Client.ViewModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace CoolParking.Client.Services
{
    public class VehiclesService : IVehiclesService
    {
        private HttpClient _client;
        private string _baseUrl = "";

        public VehiclesService(string baseUrl, HttpClient client)
        {
            _baseUrl = baseUrl;
            _client = client;
        }

        public async Task<IEnumerable<VehicleViewModel>> GetVehicles()
        {
            var vehicles = await _client.GetStringAsync($"{_baseUrl}/api/vehicles");
            return JsonConvert.DeserializeObject<IEnumerable<VehicleViewModel>>(vehicles);
        }

        public async Task<VehicleViewModel> GetVehicle(string id)
        {
            var response = await _client.GetAsync($"{_baseUrl}/api/vehicles/{id}");
            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                throw new Exception($"{response.StatusCode}: {await response.Content.ReadAsStringAsync()}");
            }
            return await response.Content.ReadAsAsync<VehicleViewModel>();
        }

        public async Task<VehicleViewModel> PostVehicle(VehicleViewModel vehicle)
        {
            var response = await _client.PostAsJsonAsync($"{_baseUrl}/api/vehicles", vehicle);
            if (response.StatusCode != System.Net.HttpStatusCode.Created)
            {
                throw new Exception($"{response.StatusCode}: {await response.Content.ReadAsStringAsync()}");
            }
            return await response.Content.ReadAsAsync<VehicleViewModel>();
        }

        public async Task DeleteVehicle(string id)
        {
            var response = await _client.DeleteAsync($"{_baseUrl}/api/vehicles/{id}");
            if (response.StatusCode != System.Net.HttpStatusCode.NoContent)
            {
                throw new Exception($"{response.StatusCode}: {await response.Content.ReadAsStringAsync()}");
            }
        }

    }
}
