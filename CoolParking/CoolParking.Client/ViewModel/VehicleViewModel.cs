﻿using CoolParking.BL.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace CoolParking.Client.ViewModel
{
    public class VehicleViewModel
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("vehicleType")]
        public VehicleType VehicleType { get; set; }

        [JsonProperty("balance")]
        public decimal Balance { get; set; }

        public override string ToString()
        {
            return $"'{Id}'  {VehicleType.ToString()}  balance = {Balance}";
        }

    }
}
